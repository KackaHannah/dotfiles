#!/bin/bash

# pls rename me or I'll die in a very spectacular fashion
set -e
if [ ! -d ".cfg" ];
then
	git init --bare .cfg
fi
config="git --git-dir=$HOME/.cfg --work-tree=$HOME"
read -p "Use git (s)sh or (h)ttps?" -n 1 test
echo
if [[ "$test" == "s" ]]; then
	remote="git@gitlab.com:/KackaHannah/dotfiles.git"
else
	remote="https://gitlab.com/KackaHannah/dotfiles.git"
fi
if $config remote | grep origin >/dev/null;
then
	$config remote remove origin
fi
$config remote add origin $remote
$config pull --set-upstream origin main
$config submodule init
$config submodule update
$config config --local status.showUntrackedFiles no
