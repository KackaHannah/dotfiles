return {
	{
		"rcarriga/nvim-dap-ui",
		dependencies = {
			"rcarriga/nvim-dap",
			"nvim-neotest/nvim-nio",
			"mfussenegger/nvim-dap-python",
		},
		config = function()
			local dap, dapui = require("dap"), require("dapui")

			require("dapui").setup()
			require("dap-python").setup("~/.virtualenvs/debugpy/bin/python")

			dap.listeners.before.attach.dapui_config = function()
				dapui.open()
			end
			dap.listeners.before.launch.dapui_config = function()
				dapui.open()
			end
			dap.listeners.before.event_terminated.dapui_config = function()
				dapui.close()
			end
			dap.listeners.before.event_exited.dapui_config = function()
				dapui.close()
			end

			vim.keymap.set("n", "<leader>dt", function()
				dap.toggle_breakpoint()
			end)
			vim.keymap.set("n", "<leader>dc", function()
				dap.continue()
			end)
		end,
	},
	{
		"jay-babu/mason-nvim-dap.nvim",
		dependencies = {
			"williamboman/mason.nvim",
			"rcarriga/nvim-dap",
		},

		config = function()
			require("mason").setup()
			require("mason-nvim-dap").setup({
				ensure_installed = {
					"debugpy",
					"clangd",
					"clang-format",
					"codelldb",
				},
				handlers = {},
			})
		end,
	},
}
