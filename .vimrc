set nocompatible
set tabstop=4 shiftwidth=4 softtabstop=4 noexpandtab smarttab smartindent autoindent
set nowrap
set cursorline number
set hlsearch incsearch ignorecase smartcase
set encoding=utf-8
set clipboard=unnamedplus
set mouse=a
set listchars=tab:\|\ ,trail:▒,extends:»,precedes:«,nbsp:░
set list
let mapleader=" "

hi cursorline cterm=none term=none ctermbg=236

syntax on
filetype on

" Set up vertical vs block cursor for insert/normal mode
if &term =~ "screen."
    let &t_ti.="\eP\e[1 q\e\\"
    let &t_SI.="\eP\e[5 q\e\\"
    let &t_EI.="\eP\e[1 q\e\\"
    let &t_te.="\eP\e[0 q\e\\"
else
    let &t_ti.="\<Esc>[1 q"
    let &t_SI.="\<Esc>[5 q"
    let &t_EI.="\<Esc>[1 q"
    let &t_te.="\<Esc>[0 q"
endif

" Set linebreak wrap for tex files
augroup FileTypeWrap
  autocmd!
  autocmd FileType plaintex,tex,markdown setlocal wrap linebreak
augroup END
